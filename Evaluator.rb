class Evaluator
    def initialize()
        @true = BoolObject.new(true)
        @false = BoolObject.new(false)
        @null = NullObject.new()
        @store = {}
    end

    def eval(node)
        case node
        when Root
            return evalRoot(node.statements)
        when ExpressionStatement
            return eval(node.expression)
        when IntLiteralExpression
            return IntObject.new(node.value)
        when BoolLiteralExpression
            return toBoolObject(node.value)
        when PrefixExpression
            right = eval(node.right)
            return evalPrefixExpression(node.operator, right)
        when InfixExpression
            return evalInfixExpression(node.operator, eval(node.left), eval(node.right))
        when BlockStatement
            return evalStatements(node.statements)
        when IfExpression
            return evalIfExpression(node)
        when ReturnStatement
            value = eval(node.returnValue)
            return ReturnValue.new(value)
        when LetStatement
            @store[node.name.value] = eval(node.value)
        when IdentifierExpression
            return evalIdentifier(node)
        when FunctionLiteralExpression
            function = FunctionObject.new()
            function.parameters = node.parameters
            function.body = node.body

            return function
        when CallExpression
            function = eval(node.function)
            arguments = evalExpressions(node.arguments)
            return evalCall(function, arguments)
        end

        return @null
    end
    
    def evalRoot(statements)
        result = nil
        for statement in statements
            result = eval(statement)

            if result.class == ReturnValue
                return result.value
            end
        end
        return result
    end

    def evalBlockStatements(blockStatement)
        result = nil
        for statement in blockStatement.statements
            result = eval(statement)

            if result.getType() == ObjectType::RETURNVALUE
                return result
            end
        end
        return result
    end

    def evalPrefixExpression(operator, right)
        case operator
        when "!"
            return evalBangOperator(right)
        when "-"
            return evalMinusPrefixOperatorExpression(right)
        else
            return @null
        end
    end

    def evalBangOperator(right)
        case right
        when @true
            return @false
        when @false
            return @true
        end

        return @false
    end

    def evalMinusPrefixOperatorExpression(right)
        if right.getType() != ObjectType::INT
            return @null
        end

        value = right.value
        return IntObject.new(-value)
    end

    def evalInfixExpression(operator, left, right)
        if left.class == IntObject && right.class == IntObject
            return evalIntInfixExpression(operator, left, right)
        end
        
        if left.class == BoolObject && right.class == BoolObject
            return evalBoolInfixExpression(operator, left, right)
        end

        return @null
    end

    def evalIntInfixExpression(operator, left, right)
        case operator
        when "+"
            return IntObject.new(left.value + right.value)
        when "-"
            return IntObject.new(left.value - right.value)
        when "*"
            return IntObject.new(left.value * right.value)
        when "/"
            return IntObject.new(left.value / right.value)
        when "<"
            return toBoolObject(left.value < right.value)
        when ">"
            return toBoolObject(left.value > right.value)
        when "=="
            return toBoolObject(left.value == right.value)
        when "!="
            return toBoolObject(left.value != right.value)
        else
            return @null
        end
    end

    def evalBoolInfixExpression(operator, left, right)
        case operator
        when "=="
            return toBoolObject(left.value == right.value)
        when "!="
            return toBoolObject(left.value != right.value)
        else
            return @null
        end
    end

    def evalIfExpression(ifExpression)
        condition = eval(ifExpression.condition)

        if isTruthly(condition)
            return evalBlockStatements(ifExpression.consequence)
        elsif ifExpression.alternative != nil
            return evalBlockStatements(ifExpression.alternative)
        end

        return @null
    end

    def evalIdentifier(identifier)
        return @store[identifier.value]
    end

    def evalExpressions(arguments)
        result = []
        for arg in arguments
            evaluated = eval(arg)
            result.push(evaluated)
        end

        return result
    end

    def evalCall(object, arguments)
        if object.class != FunctionObject
            return @null
        end

        for i in 0..arguments.length - 1
            @store[object.parameters[i].value] = arguments[i]
        end

        evaluated = evalBlockStatements(object.body)

        return unwrapReturnValue(evaluated)
    end

    def unwrapReturnValue(object)
        if object.class == ReturnValue
            return object.value
        end

        return object
    end

    def toBoolObject(value)
        if value
            return @true
        else
            return @false
        end
    end

    def isTruthly(object)
        case object
        when @true
            return true
        when @false
            return false
        when @null
            return false
        else
            return true
        end
    end
end
