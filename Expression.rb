class Expression < Node
end

class IdentifierExpression < Expression
    attr_accessor :token, :value

    def initialize(token, value)
        @token = token
        @value = value
    end

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        return @value
    end
end

class IntLiteralExpression < Expression
    attr_accessor :token, :value

    def initialize(token, value)
        @token = token
        @value = value
    end

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        return @token.literal
    end
end

class BoolLiteralExpression < Expression
    attr_accessor :token, :value

    def initialize(token, value)
        @token = token
        @value = value
    end

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        return @token.literal
    end
end

class FunctionLiteralExpression < Expression
    attr_accessor :token, :parameters, :body

    def initialize(token)
        @token = token
    end

    def tokenLiteral()
        return @tokenLiteral
    end

    def toCode()
        parameters = @parameters.map{|item| item.toCode()}
        code = "fn("
        code += parameters.join(",")
        code += ")"
        code += @body.toCode()

        return code
    end
end

class PrefixExpression < Expression
    attr_accessor :token, :operator, :right

    def initialize(token, operator)
        @token = token
        @operator = operator
    end

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        return "(" + @operator + @right.toCode() + ")"
    end
end

class InfixExpression < Expression
    attr_accessor :token, :operator, :left, :right

    def initialize(token, operator)
        @token = token
        @operator = operator
    end

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        code = "("
        code += @left.toCode()
        code += @operator
        code += @right.toCode()
        code += ")"

        return code
    end
end

class IfExpression < Expression
    attr_accessor :token, :condition, :consequence, :alternative

    def initialize(token)
        @token = token
    end

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        code = "if"
        code += "("
        code += @condition.toCode()
        code += "){"
        code += @consequence.toCode()

        if @alternative != nil
            code += "}else{"
            code += @alternative.toCode()
            code += "}"
        end

        return code
    end
end

class CallExpression < Expression
    attr_accessor :token, :function, :arguments

    def initialize(token)
        @token = token
    end
    
    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        args = @arguments.map{|item| item.toCode()}
        code = @function.toCode()
        code += "("
        code += args.join(",")
        code += ")"
        
        return code
    end
end
