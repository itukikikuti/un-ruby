class Interactive
    def start()
        puts "UN 0.0.1 (c) 2019 windblow"
        loop do
            print "> "

            lexer = Lexer.new(gets)
            parser = Parser.new(lexer)
            root = parser.parseRoot()
            evaluator = Evaluator.new()
            object = evaluator.eval(root)
            puts object.inspect()
        end
    end
end
