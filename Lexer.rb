class Lexer
    attr_reader :input, :currentChar, :nextChar, :position

    def initialize(input)
        @input = input
        @position = 0
        readChar()
    end

    def readChar()
        if @position >= @input.length
            @currentChar = "";
        else
            @currentChar = @input.chars[@position]
        end

        if @position + 1 >= @input.length
            @nextChar = ""
        else
            @nextChar = @input.chars[@position + 1]
        end

        @position += 1
    end

    def nextToken()
        skipWhiteSpace()
        token = nil

        case @currentChar
        when "="
            if @nextChar == "="
                token = Token.new(TokenType::EQ, "==")
                readChar()
            else
                token = Token.new(TokenType::ASSIGN, @currentChar)
            end
        when "+"
            token = Token.new(TokenType::PLUS, @currentChar)
        when "-"
            token = Token.new(TokenType::MINUS, @currentChar)
        when "*"
            token = Token.new(TokenType::ASTERISK, @currentChar)
        when "/"
            token = Token.new(TokenType::SLASH, @currentChar)
        when "!"
            if @nextChar == "="
                token = Token.new(TokenType::NOT_EQ, "!=")
                readChar()
            else
                token = Token.new(TokenType::BANG, @currentChar)
            end
        when ">"
            token = Token.new(TokenType::GT, @currentChar)
        when "<"
            token = Token.new(TokenType::LT, @currentChar)
        when ","
            token = Token.new(TokenType::COMMA, @currentChar)
        when ";"
            token = Token.new(TokenType::SEMICOLON, @currentChar)
        when "("
            token = Token.new(TokenType::LPAREN, @currentChar)
        when ")"
            token = Token.new(TokenType::RPAREN, @currentChar)
        when "{"
            token = Token.new(TokenType::LBRACE, @currentChar)
        when "}"
            token = Token.new(TokenType::RBRACE, @currentChar)
        when ""
            token = Token.new(TokenType::EOF, @currentChar)
        else
            if isLetter(@currentChar)
                identifier = readIdentifier()
                type = Token.lookupIdentifier(identifier)
                token = Token.new(type, identifier)
            elsif isDigit(@currentChar)
                number = readNumber()
                token = Token.new(TokenType::INT, number)
            else
                token = Token.new(TokenType::ILLEGAL, @currentChar)
            end
        end

        readChar()
        return token
    end

    def isLetter(char)
        return /[a-z]|[A-Z]|_/.match(char) != nil
    end

    def isDigit(char)
        return /[0-9]/.match(char) != nil
    end

    def readIdentifier()
        identifier = @currentChar

        while isLetter(@nextChar)
            identifier += @nextChar
            readChar()
        end

        return identifier
    end

    def readNumber()
        number = @currentChar

        while isDigit(@nextChar)
            number += @nextChar
            readChar()
        end

        return number
    end

    def skipWhiteSpace()
        while / |\t|\r|\n/.match(@currentChar) != nil
            readChar()
        end
    end
end
