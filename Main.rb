require "./Token"
require "./Lexer"
require "./Parser"
require "./Evaluator"
require "./Node"
require "./Statement"
require "./Expression"
require "./Object"
require "./Interactive"
require "./Test"

code = File.read(ARGV[0])
lexer = Lexer.new(code)
parser = Parser.new(lexer)
root = parser.parseRoot()
evaluator = Evaluator.new()
object = evaluator.eval(root)
puts object.inspect()
