class Node
    def tokenLiteral()
    end

    def toCode()
    end
end

class Root < Node
    attr_accessor :statements

    def tokenLiteral()
        if !@statements.empty?
            return @statements[0].tokenLiteral()
        else
            return ""
        end
    end

    def toCode()
        code = ""
        for statement in @statements
            code += statement.toCode()
        end
        
        return code
    end
end
