module ObjectType
    INT         = "INT"
    BOOL        = "BOOL"
    NULL        = "NULL"
    RETURNVALUE = "RETURNVALUE"
    FUNCTION    = "FUNCTION"
end

class Object
    def getType()
    end

    def inspect()
    end
end

class IntObject < Object
    attr_accessor :value

    def initialize(value)
        @value = value
    end

    def inspect()
        return @value.to_s()
    end

    def getType()
        return ObjectType::INT
    end
end

class BoolObject < Object
    attr_accessor :value

    def initialize(value)
        @value = value
    end

    def inspect()
        return value.to_s()
    end

    def getType()
        return ObjectType::BOOL
    end
end

class NullObject < Object
    def inspect()
        return "null"
    end

    def getType()
        return ObjectType::NULL
    end
end

class ReturnValue < Object
    attr_accessor :value

    def initialize(value)
        @value = value
    end

    def inspect()
        return @value.inspect()
    end

    def getType()
        return ObjectType::RETURNVALUE
    end
end

class FunctionObject < Object
    attr_accessor :parameters, :body

    def inspect()
        parameters = @parameters.map{|item| item.toCode()}
        code = "fn("
        code += parameters.join(",")
        code += ")"
        code += @body.toCode()

        return code
    end

    def getType()
        return ObjectType::FUNCTION
    end
end