module Priority
    LOWEST      = 1
    EQUALS      = 2
    LESSGREATER = 3
    SUM         = 4
    PRODUCT     = 5
    PREFIX      = 6
    CALL        = 7
end

class Parser
    attr_accessor :currentToken, :nextToken, :errors, :prefixParseFns, :infixParseFns, :infixPrioritys
    attr_reader :lexer

    def initialize(lexer)
        @lexer = lexer

        @currentToken = lexer.nextToken()
        @nextToken = lexer.nextToken()

        @errors = []

        @prefixParseFns = {
            TokenType::IDENT    => method(:parseIdentifierExpression),
            TokenType::INT      => method(:parseIntLiteralExpression),
            TokenType::TRUE     => method(:parseBoolLiteralExpression),
            TokenType::FALSE    => method(:parseBoolLiteralExpression),
            TokenType::BANG     => method(:parsePrefixExpression),
            TokenType::MINUS    => method(:parsePrefixExpression),
            TokenType::LPAREN   => method(:parseGroupedExpression),
            TokenType::IF       => method(:parseIfExpression),
            TokenType::FUNCTION => method(:parseFunctionLiteralExpression),
        }

        @infixParseFns = {
            TokenType::PLUS     => method(:parseInfixExpression),
            TokenType::MINUS    => method(:parseInfixExpression),
            TokenType::ASTERISK => method(:parseInfixExpression),
            TokenType::SLASH    => method(:parseInfixExpression),
            TokenType::EQ       => method(:parseInfixExpression),
            TokenType::NOT_EQ   => method(:parseInfixExpression),
            TokenType::LT       => method(:parseInfixExpression),
            TokenType::GT       => method(:parseInfixExpression),
            TokenType::LPAREN   => method(:parseCallExpression),
        }

        @infixPrioritys = {
            TokenType::EQ       => Priority::EQUALS,
            TokenType::NOT_EQ   => Priority::EQUALS,
            TokenType::LT       => Priority::LESSGREATER,
            TokenType::GT       => Priority::LESSGREATER,
            TokenType::PLUS     => Priority::SUM,
            TokenType::MINUS    => Priority::SUM,
            TokenType::ASTERISK => Priority::PRODUCT,
            TokenType::SLASH    => Priority::PRODUCT,
            TokenType::LPAREN   => Priority::CALL,
        }
    end

    def readToken()
        @currentToken = @nextToken
        @nextToken = lexer.nextToken()
    end

    def parseRoot()
        root = Root.new()
        root.statements = []
        
        while @currentToken.type != TokenType::EOF
            statement = parseStatement()
            if (statement != nil)
                root.statements.push(statement)
            end
            readToken()
        end

        return root
    end

    def parseParameters()
        parameters = []

        if @nextToken.type == TokenType::RPAREN
            readToken()
            return parameters
        end

        readToken()

        parameters.push(IdentifierExpression.new(@currentToken, @currentToken.literal))

        while @nextToken.type == TokenType::COMMA
            readToken()
            readToken()

            parameters.push(IdentifierExpression.new(@currentToken, @currentToken.literal))
        end

        if !expectPeek(TokenType::RPAREN)
            return nil
        end

        return parameters
    end

    def parseStatement()
        case @currentToken.type
        when TokenType::LET
            return parseLetStatement()
        when TokenType::RETURN
            return parseReturnStatement()
        else
            return parseExpressionStatement()
        end
    end

    def parseLetStatement()
        statement = LetStatement.new()
        statement.token = @currentToken

        if !expectPeek(TokenType::IDENT)
            return nil
        end

        statement.name = IdentifierExpression.new(@currentToken, @currentToken.literal)

        if !expectPeek(TokenType::ASSIGN)
            return nil
        end

        readToken()

        statement.value = parseExpression(Priority::LOWEST)
        if @nextToken.type == TokenType::SEMICOLON
            readToken()
        end

        return statement
    end

    def parseReturnStatement()
        statement = ReturnStatement.new()
        statement.token = @currentToken
        readToken()

        statement.returnValue = parseExpression(Priority::LOWEST)
        if @nextToken.type == TokenType::SEMICOLON
            readToken()
        end

        return statement
    end

    def parseExpressionStatement()
        statement = ExpressionStatement.new()
        statement.token = @currentToken

        statement.expression = parseExpression(Priority::LOWEST)

        if @nextToken.type == TokenType::SEMICOLON
            readToken()
        end

        return statement
    end

    def parseBlockStatement()
        block = BlockStatement.new()
        block.token = @currentToken
        block.statements = []

        readToken()

        while @currentToken.type != TokenType::RBRACE && @currentToken.type != TokenType::EOF
            statement = parseStatement()
            if statement != nil
                block.statements.push(statement)
            end
            readToken()
        end

        return block
    end

    def parseExpression(priority)
        prefixParseFn =  @prefixParseFns[@currentToken.type]
        if prefixParseFn == nil
            addPrefixParseFnError(@currentToken.type)
            return
        end

        leftExpression = prefixParseFn.call()

        while @nextToken.type != TokenType::SEMICOLON && priority < nextInfixPriority()
            infixParseFn = @infixParseFns[@nextToken.type]
            if infixParseFn == nil
                return leftExpression
            end

            readToken()
            leftExpression = infixParseFn.call(leftExpression)
        end

        return leftExpression
    end

    def parseIdentifierExpression()
        return IdentifierExpression.new(@currentToken, @currentToken.literal)
    end

    def parseIntLiteralExpression()
        return IntLiteralExpression.new(@currentToken, @currentToken.literal.to_i)
    end

    def parseBoolLiteralExpression()
        return BoolLiteralExpression.new(@currentToken, @currentToken.type == TokenType::TRUE)
    end

    def parseFunctionLiteralExpression()
        fn = FunctionLiteralExpression.new(@currentToken)

        if !expectPeek(TokenType::LPAREN)
            return nil
        end
        
        fn.parameters = parseParameters()

        if !expectPeek(TokenType::LBRACE)
            return nil
        end

        fn.body = parseBlockStatement()

        return fn
    end

    def parsePrefixExpression()
        expression = PrefixExpression.new(@currentToken, @currentToken.literal)

        readToken()
        expression.right = parseExpression(Priority::PREFIX)

        return expression
    end

    def parseInfixExpression(left)
        expression = InfixExpression.new(@currentToken, @currentToken.literal)
        expression.left = left

        priority = currentInfixPriority()
        readToken()
        expression.right = parseExpression(priority)

        return expression
    end

    def parseGroupedExpression()
        readToken()

        expression = parseExpression(Priority::LOWEST)

        if !expectPeek(TokenType::RPAREN)
            return nil
        end

        return expression
    end

    def parseIfExpression()
        expression = IfExpression.new(@currentToken)

        if !expectPeek(TokenType::LPAREN)
            return nil
        end

        readToken()

        expression.condition = parseExpression(Priority::LOWEST)

        if !expectPeek(TokenType::RPAREN)
            return nil
        end
        if !expectPeek(TokenType::LBRACE)
            return nil
        end

        expression.consequence = parseBlockStatement()

        if @nextToken.type == TokenType::ELSE
            readToken()
            if !expectPeek(TokenType::LBRACE)
                return nil
            end

            expression.alternative = parseBlockStatement()
        end

        return expression
    end

    def parseCallExpression(fn)
        expression = CallExpression.new(@currentToken)
        expression.function = fn
        expression.arguments = parseCallArguments()

        return expression
    end

    def parseCallArguments()
        args = []

        readToken()

        if (@currentToken.type == TokenType::RPAREN)
            return args
        end

        args.push(parseExpression(Priority::LOWEST))

        while @nextToken.type == TokenType::COMMA
            readToken()
            readToken()
            args.push(parseExpression(Priority::LOWEST))
        end

        if !expectPeek(TokenType::RPAREN)
            return nil
        end

        return args
    end

    def expectPeek(type)
        if @nextToken.type == type
            readToken()
            return true
        end

        addNextTokenError(type, @nextToken.type)
        return false
    end

    def addNextTokenError(expected, actual)
        @errors.push(actual + "ではなく" + expected + "が来なければなりません。")
    end

    def addPrefixParseFnError(type)
        @errors.push(type + "に関連付けられたPrefixParseFunctionが存在しません。")
    end

    def currentInfixPriority()
        priority = infixPrioritys[@currentToken.type]
        if priority == nil
            return Priority::LOWEST
        end

        return priority
    end

    def nextInfixPriority()
        priority = infixPrioritys[@nextToken.type]
        if priority == nil
            return Priority::LOWEST
        end

        return priority
    end
end
