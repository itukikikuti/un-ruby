class Statement < Node
end

class LetStatement < Statement
    attr_accessor :token, :name, :value

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        code = @token.literal
        code += " "
        code += @name.toCode()
        code += "="
        code += @value.toCode()
        code += ";"

        return code
    end
end

class ReturnStatement < Statement
    attr_accessor :token, :returnValue

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        code = @token.literal
        code += @returnValue.toCode()
        code += ";"
        
        return code
    end
end

class ExpressionStatement < Statement
    attr_accessor :token, :expression

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        return @expression.toCode()
    end
end

class BlockStatement < Statement
    attr_accessor :token, :statements

    def tokenLiteral()
        return @token.literal
    end

    def toCode()
        code = ""
        for statement in statements
            code += statement.toCode()
        end

        return code
    end
end
