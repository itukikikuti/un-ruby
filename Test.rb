def assertAreEqual(a, b, message)
    if a != b
        puts message + " " + a.to_s + " != " + b.to_s
    end
end

def assertFail(message)
    puts message
end

def CheckParserErrors(parser)
    if parser.errors.length == 0
        return
    end

    puts "\n" + parser.errors.join("\n")
end
