module TokenType
    ILLEGAL     = "ILLEGAL"
    EOF         = "EOF"
    IDENT       = "IDENT"
    INT         = "INT"
    ASSIGN      = "ASSIGN"
    PLUS        = "PLUS"
    MINUS       = "MINUS"
    ASTERISK    = "ASTERISK"
    SLASH       = "SLASH"
    BANG        = "BANG"
    LT          = "LT"
    GT          = "GT"
    EQ          = "EQ"
    NOT_EQ      = "NOT_EQ"
    COMMA       = "COMMA"
    SEMICOLON   = "SEMICOLON"
    LPAREN      = "LPAREN"
    RPAREN      = "RPAREN"
    LBRACE      = "LBRACE"
    RBRACE      = "RBRACE"
    FUNCTION    = "FUNCTION"
    LET         = "LET"
    IF          = "IF"
    ELSE        = "ELSE"
    RETURN      = "RETURN"
    TRUE        = "TRUE"
    FALSE       = "FALSE"
end

class Token
    @@keywords = {
        "let"       => TokenType::LET,
        "fn"        => TokenType::FUNCTION,
        "if"        => TokenType::IF,
        "else"      => TokenType::ELSE,
        "return"    => TokenType::RETURN,
        "true"      => TokenType::TRUE,
        "false"     => TokenType::FALSE,
    }

    def self.lookupIdentifier(identifier)
        if @@keywords[identifier] != nil
            return @@keywords[identifier]
        end

        return TokenType::IDENT;
    end

    attr_reader :type, :literal

    def initialize(type, literal)
        @type = type
        @literal = literal
    end
end
